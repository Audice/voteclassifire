from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import math
import matplotlib.pyplot as plt
import numpy as np
import random

PI = 3.1415926535897932384626433832795

def rastrigin(*X, Y, **kwargs):
    A = kwargs.get('A', 10)
    K = kwargs.get('K', 5)

    return A + sum([(x**2 - A * np.cos(0.5 * math.pi * x)) for x in X]) + Y * K

def concretRastrigin(X, Y, **kwargs):
    A = kwargs.get('A', 10)
    K = kwargs.get('K', 5)
    return A + (X**2 - A * np.cos(0.5 * math.pi * X)) + Y * K

def rastriginClassic(X, Y, **kwargs):
    A = kwargs.get('A', 10)
    K = kwargs.get('K', 5)
    return A + sum([(x**2 - A * np.cos(0.5 * math.pi * x) + Y * K) for x in X])

def parabaloidEleptic(X, Y, p, q, dX, dY):
    Z = []
    for i in range(0, len(X)):
        Z.append(((X[i] - dX) / p)**2 + ((Y[i] - dY)/q)**2)
    return Z

def parabaloidEleptic(X, Y, p, q, dX, dY):
    return ((X - dX) / p)**2 + ((Y - dY)/q)**2

def customPlot(X, Y):
    return X * Y

#Функция Гришагина

def a_i_j(i, yi, j, yj):
    return np.sin(PI * i * yi) * np.sin(PI * j * yj)

def b_i_j(i, yi, j, yj):
    return np.cos(PI * i * yi) * np.cos(PI * j * yj)

#X и Y - переменные
def GrihaginFunction(X, Y, A, B, C, D):
    firstSum = 0;
    for i in range(1, 7):
        for j in range(1, 7):
            firstSum = firstSum + A[i][j] * a_i_j(i, X, j, Y) + B[i][j] * b_i_j(i, X, j, Y)
    firstSum = firstSum**2
    secondSum = 0
    for i in range(1, 7):
        for j in range(1, 7):
            secondSum = secondSum + C[i][j] * a_i_j(i, X, j, Y) - D[i][j] * b_i_j(i, X, j, Y)
    secondSum = secondSum**2
    sum = (-1) * np.sqrt(firstSum + secondSum)
    return sum


if __name__ == '__main__':
    leftBorderX = 0
    rightBorderX = 1
    leftBorderY = 0
    rightBorderY = 1
    bottomZ = 0
    topZ = 1


    pointsCount = 50


    X = np.linspace(leftBorderX, rightBorderX, pointsCount)
    Y = np.linspace(leftBorderY, rightBorderY, pointsCount)

    #Построение сетки для функции F(x,y)
    X, Y = np.meshgrid(X, Y)

    #Z = rastrigin(X, Y=Y, A=10, K=5)
    #Z = parabaloidEleptic(X, Y, 1, 1, 0.5, 0.5)

    curSeed = 1
    minHH_Count = 3
    maxHH_Count = 20
    rng = np.random.RandomState(curSeed)
    A = []
    for i in range(0, maxHH_Count):
        A.append(-1 + rng.rand(maxHH_Count) * 2.0)
    B = []
    for i in range(0, maxHH_Count):
        B.append(-1 + rng.rand(maxHH_Count) * 2.0)
    C = []
    for i in range(0, maxHH_Count):
        C.append(-1 + rng.rand(maxHH_Count) * 2.0)
    D = []
    for i in range(0, maxHH_Count):
        D.append(-1 + rng.rand(maxHH_Count) * 2.0)



    Z = GrihaginFunction(X, Y, A, B, C, D)

    #Построение графика
    fig = plt.figure(figsize=(8, 6))
    fig.canvas.set_window_title('Точки без перемешивания')
    ax = fig.gca(projection='3d')
    ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.plasma, linewidth=0, alpha = 0.2, antialiased=False)
    #for i in range(pointsCount):
        #ax.plot(ChoiceX[i], ChoiceY[i], ChoiceZ[i], 'bo', markersize=10, alpha=1)



    plt.show()
